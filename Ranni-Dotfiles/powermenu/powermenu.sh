#!/bin/bash

menu="$(echo -e "Lock\nLogout\nShutdown\nReboot\nSuspend\nHibernate" | rofi -dmenu -p "Power Menu" -width 15 -lines 6 -padding 20 -location 0 -yoffset 60 -xoffset 50)"

case "$menu" in
	"Lock") i3lock ;;
	"Logout") i3-msg exit ;;
	"Shutdown") systemctl poweroff ;;
	"Reboot") systemctl reboot ;;
	"Suspend") systemctl suspend ;;
	"Hibernate") systemctl hibernate ;;
esac
